const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const axios = require('axios');

app.use(express.static('build'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

// CORS functionality
const allowCrossDomain = (req, res, next) => {
	res.header('Access-Control-Allow-Origin', "*");
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	res.header('Access-Control-Allow-Credentials', 'true');
	next();
};
app.use(allowCrossDomain);

//middleware to resolve https and http error.
app.get('/api/', function(req,res) {
	const url = `http://www.recipepuppy.com/api/?p=${req.query.p}&q=${req.query.q}`;
	axios.get(url)
		.then(response => {
			console.log('response:' , response.data);
			res.send(response.data);
		})
		.catch(err => res.send(err.message));
});


module.exports = app;
