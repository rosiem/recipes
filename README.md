# Recipe Search Technical Test

Create a recipe finder app using the RecipePuppy API: http://www.recipepuppy.com/about/api/ 

### Requirements
* As the user types in a search query for the name of a recipe, search results should be displayed in a list (i.e. as the user types, the list may update)
* The first 20 results of a search must be displayed.
* You may use a framework like create react app if this suits


### Assessment criteria
1. How well the requirements above are met
2. Code quality
3. The simplicity and extensibility of the approach taken
4. Ability to solve unexpected problems, and how those are documented. This app uses the recipepuppy.com API to populate a list of recipes.


##  3. Usage

### `npm run start:dev`
