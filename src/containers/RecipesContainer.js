// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Header, Image, Container } from 'semantic-ui-react'
import { loadRecipes } from '../state/actions/recipes';
import SearchInput from "../components/SearchInput";

type Props = {
	loadRecipes: Function,
	recipes: Recipe[],
};

class RecipesContainer extends Component<Props, *> {
	componentDidMount = () => {
		this.props.loadRecipes('');
	};
	render() {
		return (
			<Container>
				<SearchInput onChange={this.props.loadRecipes} onSubmit={this.props.loadRecipes} />
				<Table basic='very' striped>
					<Table.Body>
						{this.props.recipes.map((recipe, idx) => (
							<Table.Row key={idx} verticalAlign='top'>
								<Table.Cell>
									<Header as='h4' image>
										<Image src={recipe.thumbnail} rounded size='mini' />
										<Header.Content>
											{recipe.title}
											<Header.Subheader>{recipe.ingredients}</Header.Subheader>
										</Header.Content>
									</Header>
								</Table.Cell>
							</Table.Row>
						))}
					</Table.Body>
				</Table>
			</Container>
		)
	}
}

export default connect(({ recipes }) => ({ recipes: recipes.recipes || [] }), { loadRecipes })(RecipesContainer);

