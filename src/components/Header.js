import React from 'react'
import { Container, Menu } from 'semantic-ui-react'

const Header = () => (
	<div>
		<Menu inverted>
			<Container>
				<Menu.Item as='a' header>
					Recipe finder
				</Menu.Item>
				<Menu.Item as='a'>Home</Menu.Item>
			</Container>
		</Menu>
	</div>
);

export default Header;
