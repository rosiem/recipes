// @flow
import React, { Component } from 'react'
import { Form } from 'semantic-ui-react'

const styles = {
	fullWidth: {
		display: 'flex',
		width: '100%'
	},
	container: {
		display: 'flex',
		padding: '20px 0',
		width: '100%'
	},
};

type Props = {
	onChange: Function,
	onSubmit: Function,
};

class SearchInput extends Component<Props, *> {
	state = { search: '' };
	handleChange = (e, { name, value }) => {
		this.setState({ [name]: value });
		this.props.onChange(value);
	};

	handleSubmit = () => {
		const { search } = this.state;
		this.setState({ search });
		this.props.onSubmit(search);
	};

	render() {
		const { search } = this.state;
		return (
			<div style={styles.container}>
				<Form style={styles.fullWidth} onSubmit={this.handleSubmit}>
					<Form.Group style={styles.fullWidth}>
						<Form.Input
							placeholder='Search for ...'
							name='search'
							value={search}
							onChange={this.handleChange}
							width={10}
						/>
						<Form.Button content='Search' width={2} />
					</Form.Group>
				</Form>
			</div>
		)
	}
}

export default SearchInput
