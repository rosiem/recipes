/* eslint-env jest */
import React from 'react';
import { shallow } from 'enzyme';
import SearchInput from './Header';

const props = {
	onChange: () => {},
	onSubmit: () => {},
};

describe('Search Input', () => {
	it('renders without error', () => {
		const wrapper = shallow(<SearchInput {...props} />);
		expect(wrapper).toMatchSnapshot();
	});
});
