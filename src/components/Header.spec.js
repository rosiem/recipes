/* eslint-env jest */
import React from 'react';
import { shallow } from 'enzyme';
import Header from './Header';

describe('Header', () => {
	it('renders without error', () => {
		const wrapper = shallow(<Header />);
		expect(wrapper).toMatchSnapshot();
	});
});
