// @flow

export type Error = {
	message: string,
	key?: string,
	data?: { property: string, message: string }[],
};
