import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';

import Header from './components/Header';
import RecipesContainer from './containers/RecipesContainer';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <RecipesContainer />
      </div>
    );
  }
}

export default App;
