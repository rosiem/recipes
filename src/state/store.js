import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

import reducers from './reducers';
import sagas from './effects';

const sagaMiddleware = createSagaMiddleware();

const middleware = [sagaMiddleware];

const store = createStore(reducers, composeWithDevTools(applyMiddleware(...middleware)));

sagas.forEach(saga => sagaMiddleware.run(saga));

export default store;
