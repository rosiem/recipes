// @flow
import { constants, type Action } from '../actions/recipes';

export type State = {
	isLoading: boolean,
	error: ?Error,
	recipes: Recipe[],
};

const initialState: State = {
	isLoading: false,
	error: null,
	recipes: [],
};

export default function reducer(state: State = initialState, action: Action): State {
	switch (action.type) {
		case constants.LoadRecipes:
			return { ...state, isLoading: true };

		case constants.LoadRecipesSuccessful: {
			const recipes = action.payload;
			return {
				...state,
				recipes,
				error: null,
				isLoading: false,
			};
		}

		case constants.LoadRecipesFailure:
			return { ...state, error: action.payload, isLoading: false };

		default:
			return state;
	}
}
