// @flow
import recipesSagas from './recipes';

export default [
	...recipesSagas,
];
