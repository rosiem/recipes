// @flow
import { put, call } from 'redux-saga/effects';

import { get } from '../../helpers/http';
import watch from '../../helpers/watch';
import { API_BASE_URL_RECIPES } from '../../constants/apis';
import { loadRecipesSuccessful, loadRecipesFailure,  constants } from '../actions/recipes';

// utility function to 'sleep' some time
const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

function* loadRecipes({ payload }) {
	try {
		// debounce
		yield call(delay, 500);
		const firstTen = yield call(get, `${API_BASE_URL_RECIPES}q=${payload}&p=1`);
		const secondTen = yield call(get, `${API_BASE_URL_RECIPES}q=${payload}&p=2`);
		yield put(loadRecipesSuccessful(firstTen.data.results.concat(secondTen.data.results)));
	} catch (error) {
		yield put(loadRecipesFailure({ message: 'Failed to load recipes' }));
	}
}

export default [
	watch(constants.LoadRecipes, loadRecipes, true),
];
