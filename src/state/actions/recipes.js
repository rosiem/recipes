// @flow

const LoadRecipes = 'LOAD RECIPES';
const loadRecipes = (searchTerm: string) => ({ type: LoadRecipes, payload: searchTerm });

const LoadRecipesFailure = 'RECIPES LOAD FAILURE';
const loadRecipesFailure = (error: Error) => ({
	type: LoadRecipesFailure,
	payload: error,
});

const LoadRecipesSuccessful = 'RECIPES LOAD SUCCESSFUL';
const loadRecipesSuccessful = (recipes: Recipe[]) => ({
	type: LoadRecipesSuccessful,
	payload: recipes
});

export {
	loadRecipes,
	loadRecipesFailure,
	loadRecipesSuccessful,
};

export const constants = {
	LoadRecipes,
	LoadRecipesFailure,
	LoadRecipesSuccessful,
};
