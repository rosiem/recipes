import { takeEvery } from 'redux-saga/effects';

export default function watch(pattern, saga) {
	return function* patternWatch() {
		yield takeEvery(pattern, saga);
	}
};

